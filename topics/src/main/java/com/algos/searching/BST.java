package com.algos.searching;

import java.util.LinkedList;
import java.util.Queue;

/**
 * @author Harit Himanshu
 */
public class BST<Key extends Comparable<Key>, Value> {
    private Node root;

    private class Node {
        private Key key;
        private Value value;
        private Node left;
        private Node right;
        private int N;

        private Node(final Key key, final Value value, final int n) {
            this.key = key;
            this.value = value;
            N = n;
        }
    }

    public int size() {
        return size(root);
    }

    private int size(final Node node) {
        if (node == null) {
            return 0;
        }
        return node.N;
    }

    public Value get(final Key key) {
        return get(root, key);
    }

    private Value get(final Node node, final Key key) {
        if (node == null) {
            return null;
        }
        final int cmp = key.compareTo(node.key);
        if (cmp < 0) {
            return get(node.left, key);
        } else if (cmp > 0) {
            return get(node.right, key);
        }
        return node.value;
    }

    public void put(final Key key, final Value value) {
        put(root, key, value);
    }

    private Node put(final Node node, final Key key, final Value value) {
        if (node == null) {
            final Node newNode = new Node(key, value, 1);
            if (root == null) {
                root = newNode;
            }
            return newNode;
        }
        final int cmp = key.compareTo(node.key);
        if (cmp < 0) {
            node.left = put(node.left, key, value);
        } else if (cmp > 0) {
            node.right = put(node.right, key, value);
        } else {
            node.value = value;
        }
        node.N = size(node.left) + size(node.right) + 1;
        return node;
    }

    public Key min() {
        return min(root).key;
    }

    private Node min(final Node node) {
        if (node.left != null) {
            return min(node.left);
        }
        return node;
    }

    public Key max() {
        return max(root).key;
    }

    private Node max(final Node node) {
        if (node.right != null) {
            return max(node.right);
        }
        return node;
    }

    public Key floor(final Key key) {
        final Node node = floor(root, key);
        if (node == null) {
            return null;
        }
        return node.key;
    }

    private Node floor(final Node node, final Key key) {
        if (node == null) {
            return null;
        }
        final int cmp = key.compareTo(node.key);
        if (cmp == 0) {
            return node;
        } else if (cmp < 0) {
            return floor(node.left, key);
        }
        final Node t = floor(node.right, key);
        if (t != null) {
            return t;
        }
        return node;
    }

    public Key select(final int k) {
        return select(root, k).key;
    }

    private Node select(final Node node, final int k) {
        if (node == null) {
            return null;
        }
        final int t = size(node.left);
        if (k < t) {
            return select(node.left, k);
        } else if (k > t) {
            return select(node.right, k - t - 1);
        }
        return node;
    }

    public int rank(final Key key) {
        return rank(root, key);
    }

    private int rank(final Node node, final Key key) {
        if (node == null) {
            return 0;
        }
        final int cmp = key.compareTo(node.key);
        if (cmp < 0) {
            return rank(node.left, key);
        } else if (cmp > 0) {
            return 1 + size(node.left) + rank(node.right, key);
        }
        return size(node.left);
    }

    public void deleteMin() {
        root = deleteMin(root);
    }

    // reshapes the tree
    private Node deleteMin(final Node node) {
        if (node == null) {
            return null;
        }
        if (node.left == null) {
            return node.right;
        }
        node.left = deleteMin(node.left);
        node.N = size(node.left) + size(node.right) + 1;
        return node;
    }

    public void deleteMax() {
        root = deleteMax(root);
    }

    // reshapes the tree
    private Node deleteMax(final Node node) {
        if (node == null) {
            return null;
        }
        if (node.right == null) {
            return node.left;
        }
        node.right = deleteMax(node.right);
        node.N = size(node.left) + size(node.right) + 1;
        return node;
    }

    public void delete(final Key key) {
        root = delete(root, key);
    }

    private Node delete(Node node, final Key key) {
        if (node == null) {
            return null;
        }
        final int cmp = key.compareTo(node.key);
        if (cmp < 0) {
            node.left = delete(node.left, key);
        } else if (cmp > 0) {
            node.right = delete(node.right, key);
        } else {
            if (node.left == null) {
                return node.right;
            }
            if (node.right == null) {
                return node.left;
            }
            final Node t = node;
            node = min(t.right);
            node.right = deleteMin(t.right);
            node.left = t.left;
        }
        node.N = size(node.left) + size(node.right) + 1;
        return node;
    }

    public Iterable<Key> keys() {
        return keys(min(), max());
    }

    public Iterable<Key> keys(final Key lo, final Key hi) {
        final Queue<Key> queue = new LinkedList<>();
        keys(root, queue, lo, hi);
        return queue;
    }

    private void keys(final Node node, final Queue<Key> queue, final Key lo, final Key hi) {
        if (node == null) {
            return;
        }
        final int cmplo = lo.compareTo(node.key);
        final int cmphi = hi.compareTo(node.key);
        if (cmplo < 0) {
            keys(node.left, queue, lo, hi);
        }
        if (cmplo <= 0 && cmphi >= 0) {
            queue.add(node.key);
        }
        if (cmphi > 0) {
            keys(node.right, queue, lo, hi);
        }
    }
}
