package com.algos.searching;

/**
 * @author Harit Himanshu
 */
public class RedBlackBST<Key extends Comparable<Key>, Value> {
    private Node root;
    private static final boolean RED = true;
    private static final boolean BLACK = false;

    private class Node {
        private Key key;
        private Value value;
        private int N;
        private Node left, right;
        private boolean color;

        Node(final Key key, final Value value, final int n, final boolean color) {
            this.key = key;
            this.value = value;
            N = n;
            this.color = color;
        }

    }

    public int size() {
        return size(root);
    }

    private int size(final Node x) {
        if (x == null) {
            return 0;
        }
        return 1 + size(x.left) + size(x.right);
    }

    private boolean isRed(final Node x) {
        return x.color == RED;
    }

    private Node rotateLeft(final Node h) {
        final Node x = h.right;
        h.right = x.left;
        x.left = h;
        x.N = h.N;
        x.color = h.color;
        h.color = RED;
        h.N = 1 + size(h.left) + size(h.right);
        return x;
    }

    private Node rotateRight(final Node h) {
        final Node x = h.left;
        h.left = x.right;
        x.right = h;
        x.color = h.color;
        h.color = RED;
        x.N = h.N;
        h.N = 1 + size(h.left) + size(h.right);
        return x;
    }

    private void flipColors(final Node h) {
        h.color = RED;
        h.left.color = BLACK;
        h.right.color = BLACK;
    }

    public void put(final Key key, final Value value) {
        root = put(root, key, value);
        root.color = BLACK;
    }

    private Node put(Node h, final Key key, final Value value) {
        if (h == null) {
            return new Node(key, value, 0, RED);
        }
        final int cmp = key.compareTo(h.key);
        if (cmp < 0) {
            h.left = put(h.left, key, value);
        } else if (cmp > 0) {
            h.right = put(h.right, key, value);
        } else {
            h.value = value;
        }

        if (isRed(h.right) && !isRed(h.left)) {
            h = rotateLeft(h);
        }
        if (isRed(h.left) && isRed(h.left.left)) {
            h = rotateRight(h);
        }
        if (isRed(h.left) && isRed(h.right)) {
            flipColors(h);
        }
        h.N = 1 + size(h.left) + size(h.right);
        return h;
    }
}
