package com.algos.sorting;

/**
 * @author Harit Himanshu
 */
public class HeapSort extends AbstractSort {
    public void sort(final Comparable[] a) {
        int N = a.length;

        // if you use sink technique to build heap
        // this is 20% faster than swim technique
        // Q4. Pg 328 Algorithms - Sedgewick, Wayne
        for (int k = N / 2; k >= 1; k--) {
            sink(a, k, N);
        }

        // if you use swim technique to build heap
        /*
        for (int k = 1; k <= N; k++) {
            swim(a, k);
        }
        */

        // sorting
        while (N > 1) {
            exch(a, 1, N--);
            sink(a, 1, N);
        }
    }

    private void swim(final Comparable[] a, int k) {
        while (k > 1 && less(a, k/2, k)) {
            exch(a, k/2, k);
            k = k/2;
        }
    }

    private void sink(final Comparable[] a, int k, final int N) {
        while (2 * k <= N) {
            int j = 2 * k;
            if (j < N && less(a, j, j + 1)) {
                j += 1;
            }
            if (! less(a, k, j)) {
                break;
            }
            exch(a, k, j);
            k = j;
        }
    }

    // to support off by one indices
    protected void exch(final Comparable[] a, final int i, final int j) {
        final Comparable t = a[i - 1];
        a[i - 1] = a[j - 1];
        a[j - 1] = t;
    }

    // to support off by one indices
    private static boolean less(final Comparable[] a, final int i, final int j) {
        return a[i - 1].compareTo(a[j - 1]) < 0;
    }

}
