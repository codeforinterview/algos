package com.algos.sorting;

/**
 * @author Harit Himanshu
 *         Reference: http://www.sorting-algorithms.com/shell-sort
 */
public class ShellSort extends AbstractSort {
    public void sort(final Comparable[] a) {
        int h = 1;
        while (h < a.length / 3) {
            h = 3 * h + 1;
        }

        while (h >= 1) {
            for (int i = h; i < a.length; i++) {
                for (int j = i; j >= h && less(a[j], a[j - h]); j -= h) {
                    exch(a, j, j - h);
                }
            }
            h = h / 3;
        }
    }
}
