package com.algos.sorting;

/**
 * @author Harit Himanshu
 */
public class InsertionSort extends AbstractSort {
    public void sort(final Comparable[] a) {
        for (int i = 1; i < a.length; i++) {
            for (int j = i; j > 0 && less(a[j], a[j - 1]); j--) {
                exch(a, j, j - 1);
            }
        }
    }
}
