package com.algos.string;

import java.util.LinkedList;
import java.util.Queue;

/**
 * User: harit
 */
public class Trie<Value> {
    private static final int R = 256;
    private Node root;

    private static class Node {
        private Object value;
        private Node[] next = new Node[R];
    }

    public void put(final String key, final int value) {
        put(root, key, value, 0);
    }

    private Node put(Node node, final String key, final int value, int d) {
        if (node == null) {
            node = new Node();
        }

        if (d == key.length()) {
            node.value = value;
            return node;
        }

        char c = key.charAt(d);
        node.next[c] = put(node.next[c], key, value, d + 1);
        return node;
    }

    public Value get(final String key) {
        Node node = get(root, key, 0);
        if (node == null) {
            return null;
        }
        //noinspection unchecked
        return (Value) node.value;
    }

    public Node get(final Node node, String key, int d) {
        if (node == null) {
            return null;
        }
        if (d == key.length()) {
            return node;
        }
        char c = key.charAt(d);
        return get(node.next[c], key, d + 1);
    }

    public Iterable<String> keys() {
        return keysWithPrefix("");
    }

    private Iterable<String> keysWithPrefix(String pre) {
        final Queue<String> q = new LinkedList<>();
        final Node node = get(root, pre, 0);
        collect(node, pre, q);
        return q;
    }

    private void collect(Node node, String pre, Queue<String> q) {
        if (node == null) {
            return;
        }
        if (node.value != null) {
            q.add(pre);
        }
        for (char c = 0; c < R; c++) {
            collect(node.next[c], pre + c, q);
        }
    }

    public Iterable<String> keysThatMatch(String pat) {
        final Queue<String> q = new LinkedList<>();
        collect(root, pat, "", q);
        return q;
    }

    private void collect(final Node node, final String pat, final String pre, final Queue<String> q) {
        if (node == null) {
            return;
        }
        final int d = pre.length();
        if (d == pat.length() && node.value != null) {
            q.add(pre);
        }
        if (d == pat.length()) {
            return;
        }
        final char next = pre.charAt(d);
        for (char c = 0; c < R; c++) {
            if (next == '.' || next == c) {
                collect(node.next[c], pat, pre + c, q);
            }
        }
    }

    private String longestPrefixOf(final String string) {
        final int length = search(root, string, 0, 0);
        return string.substring(0, length);
    }

    private int search(final Node node, final String key, final int d, int length) {
        if (node == null) {
            return length;
        }
        if (node.value != null) {
            length = d;
        }
        if (d == key.length()) {
            return length;
        }
        final char c = key.charAt(d);
        return search(root, key, d + 1, length);
    }
}
