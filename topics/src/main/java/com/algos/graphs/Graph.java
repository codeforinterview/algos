package com.algos.graphs;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

// data set: "http://algs4.cs.princeton.edu/41undirected/tinyG.txt"
public class Graph {
    private final int V;
    private int E;
    private List<List<Integer>> adj;


    public Graph(final int V) {
        this.V = V;
        E = 0;
        adj = new ArrayList<>();
        for (int v = 0; v < V; v++) {
            adj.add(v, new ArrayList<Integer>());
        }
    }

    public Graph(final BufferedReader reader) throws IOException {
        this(Integer.parseInt(reader.readLine()));
        final int E = Integer.parseInt(reader.readLine());

        for (int i = 0; i < E; i++) {
            //add edge
            final String[] edge = reader.readLine().split("\\s+");
            final int v = Integer.parseInt(edge[0]);
            final int w = Integer.parseInt(edge[1]);
            addEdge(v, w);
        }
    }

    int V() {
        return V;
    }

    int E() {
        return E;
    }

    void addEdge(final int v, final int w) {
        adj.get(v).add(w);
        adj.get(w).add(v);
        E++;
    }

    Iterable<Integer> adj(final int v) {
        return adj.get(v);
    }

    public static int degree(final Graph G, final int v) {
        int degree = 0;
        for (final int adj : G.adj(v)) {
            degree++;
        }
        return degree;
    }

    public static int maxDegree(final Graph G) {
        int max = 0;
        for (int v = 0; v < G.V(); v++) {
            if (degree(G, v) > max) {
                max = degree(G, v);
            }
        }
        return max;
    }

    public static int averageDegree(final Graph G) {
        return 2 * G.E() / G.V();
    }

    public static int numberOfSelfLoops(final Graph G) {
        int selfLoops = 0;
        for (int v = 0; v < G.V(); v++) {
            for (final int w : G.adj(v)) {
                if (v == w) {
                    selfLoops++;
                }
            }
        }
        return selfLoops / 2; //each edge counted twice
    }

    public String toString() {
        String s = V + " vertices, " + E + " edges\n";
        for (int v = 0; v < V; v++) {
            s += v + ": ";
            for (final int w : adj(v)) {
                s += w + " ";
            }
            s += "\n";
        }
        return s;
    }

    public static void main(final String args[]) throws IOException {
        final Graph G = new Graph(new BufferedReader(new FileReader(new File("/Users/hhimanshu/Downloads/tinyG.txt"))));
        System.out.println(G.toString());
    }
}
