package com.algos.sorting;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;


/**
 * @author Harit Himanshu
 */
public class MaxPQTest {
    @Test
    public void testIsEmpty() throws Exception {
        final MaxPQ<Integer> pq = new MaxPQ<>(1);
        assertTrue(pq.isEmpty());

        pq.insert(1);
        assertFalse(pq.isEmpty());
    }

    @Test
    public void testSize() throws Exception {
        final MaxPQ<String> pq = new MaxPQ<>(2);
        assertEquals(0, pq.size());

        pq.insert("first");
        assertEquals(1, pq.size());

        pq.insert("second");
        assertEquals(2, pq.size());
    }

    @Test
    public void testDelMax() throws Exception {
        final MaxPQ<Integer> pq = new MaxPQ<>(4);
        pq.insert(1);
        pq.insert(2);
        pq.insert(3);
        pq.insert(4);

        assertEquals(pq.delMax(), Integer.valueOf(4));
        assertEquals(pq.delMax(), Integer.valueOf(3));
        assertEquals(pq.delMax(), Integer.valueOf(2));
        assertEquals(pq.delMax(), Integer.valueOf(1));

        assertTrue(pq.isEmpty());
    }
}
