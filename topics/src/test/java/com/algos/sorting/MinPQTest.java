package com.algos.sorting;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

/**
 * @author Harit Himanshu
 */
public class MinPQTest {
    @Test
    public void testIsEmpty() throws Exception {
        final MinPQ<Integer> pq = new MinPQ<>(1);
        assertTrue(pq.isEmpty());

        pq.insert(1);
        assertFalse(pq.isEmpty());
    }

    @Test
    public void testSize() throws Exception {
        final MinPQ<String> pq = new MinPQ<>(2);
        assertEquals(0, pq.size());

        pq.insert("first");
        assertEquals(1, pq.size());

        pq.insert("second");
        assertEquals(2, pq.size());
    }

    @Test
    public void testDelMin() throws Exception {
        final MinPQ<Integer> pq = new MinPQ<>(4);
        pq.insert(4);
        pq.insert(3);
        pq.insert(2);
        pq.insert(1);

        assertEquals(pq.delMin(), Integer.valueOf(1));
        assertEquals(pq.delMin(), Integer.valueOf(2));
        assertEquals(pq.delMin(), Integer.valueOf(3));
        assertEquals(pq.delMin(), Integer.valueOf(4));
    }
}
