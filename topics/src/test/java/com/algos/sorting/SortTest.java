package com.algos.sorting;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

/**
 * @author Harit Himanshu
 */
public class SortTest {
    private AbstractSort sortClass;

    @Before
    public void setup() {
//        sortClass = new SelectionSort();
//        sortClass = new InsertionSort();
//        sortClass = new ShellSort();
//        sortClass = new MergeSort();
        sortClass = new QuickSort();
//        sortClass = new HeapSort();
    }

    @Test(expected = NullPointerException.class)
    public void testSortWithNullInput() {
        sortClass.sort(null);
    }

    @Test
    public void testSortWithOneInput(){
        final Integer[] a = {1};
        sortClass.sort(a);
        assertEquals(a[0], Integer.valueOf(1));
    }

    @Test
    public void testSortWithMoreThanOneInput() {
        final Integer[] a = {4, 3, 2, 1};
        sortClass.sort(a);
        assertEquals(a[0], Integer.valueOf(1));
        assertEquals(a[a.length - 1], Integer.valueOf(4));
    }

    @Test
    public void testWithStringInput() {
        final String[] a = {"b", "a", "c"};
        sortClass.sort(a);
        assertEquals("a", a[0]);
        assertEquals("c", a[a.length - 1]);
    }
}
