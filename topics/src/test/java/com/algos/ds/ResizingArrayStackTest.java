package com.algos.ds;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;


/**
 * @author Harit Himanshu
 */
public class ResizingArrayStackTest {

    private ResizingArrayStack<Integer> stack;

    @Before
    public void setup() {
        stack = new ResizingArrayStack<>();
    }

    @Test
    public void testIsEmpty() throws Exception {
        assertTrue(stack.isEmpty());

        stack.push(1);
        assertFalse(stack.isEmpty());
    }

    @Test
    public void testSizeWithPush() throws Exception {
        assertEquals(0, stack.size());

        stack.push(1);
        assertEquals(1, stack.size());

        stack.push(2);
        assertEquals(2, stack.size());
    }

    @Test
    public void testSizeWithPushAndPop() throws Exception {
        stack.push(1);
        assertEquals(1, stack.size());

        stack.push(2);
        stack.push(3);
        stack.push(4);

        stack.pop();
        stack.pop();
        stack.pop();

        assertEquals(1, stack.size());

    }
}
