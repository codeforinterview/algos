package com.algos.searching;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import org.junit.Test;

/**
 * @author Harit Himanshu
 */
public class BSTTest {

    private final BST<Integer, Integer> bstree;

    public BSTTest() {
        bstree = new BST<>();
        /*
            creating a BST like following
                            6
                        /        \
                    4               8
                  /    \         /    \
                2       5       7       9
              /   \
            1       3

         */
        bstree.put(6, 6);
        bstree.put(4, 4);
        bstree.put(2, 2);
        bstree.put(5, 5);
        bstree.put(1, 1);
        bstree.put(3, 3);
        bstree.put(8, 8);
        bstree.put(7, 7);
        bstree.put(9, 9);
    }

    @Test
    public void testSize() throws Exception {
        assertEquals(9, bstree.size());
    }

    @Test
    public void testGet() throws Exception {
        assertEquals(bstree.get(1), Integer.valueOf(1));
        assertEquals(bstree.get(5), Integer.valueOf(5));
        assertEquals(bstree.get(7), Integer.valueOf(7));
        assertEquals(bstree.get(6), Integer.valueOf(6));
        assertNull(bstree.get(10));
    }

    @Test(expected = NullPointerException.class)
    public void testPutNullKey() throws Exception {
        bstree.put(null, 1);
    }

    @Test
    public void testNullValuesAllowed() {
        bstree.put(1, null);
        assertNull(bstree.get(1));
    }

    @Test
    public void testUpdateValue() {
        bstree.put(1, 2);
        assertEquals(bstree.get(1), Integer.valueOf(2));
    }

    @Test
    public void testMin() {
        assertEquals(bstree.min(), Integer.valueOf(1));
    }

    @Test
    public void testMax() {
        assertEquals(bstree.max(), Integer.valueOf(9));
    }

    @Test
    public void testFloor() {
        assertEquals(bstree.floor(10), Integer.valueOf(9));
        assertNull(bstree.floor(0));
    }

    @Test
    public void testSelect() {
        assertEquals(bstree.select(0), Integer.valueOf(1));
        assertEquals(bstree.select(5), Integer.valueOf(6));
        assertEquals(bstree.select(7), Integer.valueOf(8));
    }

    @Test
    public void testRank() {
        // 0 1 2 3 4 5 6 7 8 # array index
        // 1 2 3 4 5 6 7 8 9 # bstree keys

        assertEquals(5, bstree.rank(6));
        assertEquals(0, bstree.rank(1));
        assertEquals(8, bstree.rank(9));
        assertEquals(0, bstree.rank(-10));
    }

    @Test
    public void testDeleteMin() {
        final int initialTreeSize = bstree.size();
        bstree.deleteMin();
        assertEquals(bstree.min(), Integer.valueOf(2));
        assertEquals(bstree.size(), initialTreeSize - 1);
    }

    @Test
    public void testDeleteMax() {
        final int initialTreeSize = bstree.size();
        bstree.deleteMax();
        assertEquals(bstree.max(), Integer.valueOf(8));
        bstree.deleteMax();
        assertEquals(bstree.max(), Integer.valueOf(7));
        assertEquals(bstree.size(), initialTreeSize - 2);
    }

    @Test
    public void testDelete() {
        final int initialTreeSize = bstree.size();

        bstree.delete(6);
        assertNull(bstree.get(6));
        assertEquals(bstree.size(), initialTreeSize - 1);

        bstree.delete(1);
        assertNull(bstree.get(1));
        assertEquals(bstree.min(), Integer.valueOf(2));
        assertEquals(bstree.size(), initialTreeSize - 2);

        bstree.delete(9);
        assertNull(bstree.get(9));
        assertEquals(bstree.max(), Integer.valueOf(8));
        assertEquals(bstree.size(), initialTreeSize - 3);
    }

    @Test
    public void testKeys() {
        int fullRangeCount = 0;
        for (final Integer ignored : bstree.keys()) {
            fullRangeCount++;
        }
        assertEquals(9, fullRangeCount);

        int partialRangeCount = 0;
        for (final Integer ignored : bstree.keys(5, 9)) {
            partialRangeCount++;
        }
        assertEquals(5, partialRangeCount);
    }
}
