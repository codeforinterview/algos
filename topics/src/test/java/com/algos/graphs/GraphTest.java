package com.algos.graphs;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import org.junit.Test;

public class GraphTest {

    @Test
    public void testTinyG() throws IOException {
        final Graph g = new Graph(getBufferedReaderFor("tinyG.txt"));
        System.out.println(g.toString());
    }

    private static BufferedReader getBufferedReaderFor(final String filename) {
        return new BufferedReader(new InputStreamReader(GraphTest.class.getResourceAsStream("/graphs/" + filename)));
    }
}
