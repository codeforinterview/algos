package com.algos.questions.crackcode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * User: harit
 */
public class StringPermutation {

    public static void main(String[] args) {
        List<String> permuted = permute("abc");
        System.out.println("length:" + permuted.size());
        System.out.println(permuted);
    }

    public static List<String> permute(String input) {
        if (input.length() == 1) {
            return Arrays.asList(input);
        }
        final char v = input.charAt(0);
        final List<String> permutedStrings = permute(input.substring(1, input.length()));
        final List<String> result = new ArrayList<>();
        for (String permuted : permutedStrings) {
            for (int i = 0; i <= permuted.length(); i++) {
                result.add(insertChar(permuted, v, i));
            }
        }
        return result;
    }

    private static String insertChar(String permuted, char v, int i) {
        return permuted.substring(0, i) + v + permuted.substring(i, permuted.length());
    }
}
