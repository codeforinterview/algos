package com.algos.questions.crackcode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * User: harit
 */
public class Powerset {

    public static void main(String[] args) {
        List<List<String>> allSubsets = powerSet(Arrays.asList(1, 2, 3, 4, 5, 6));
        System.out.println("Size: " + allSubsets.size());
        for (List<String> subsets : allSubsets) {
            System.out.print(subsets);
            System.out.print(" - ");
        }
        System.out.println();
    }

    private static List<List<String>> powerSet(List<Integer> values) {
        final List<List<String>> allSubsets = powerSet(values, 0);
        allSubsets.add(Arrays.asList(""));
        return allSubsets;
    }

    private static List<List<String>> powerSet(List<Integer> values, int index) {
        if (index == values.size()) {
            return Collections.emptyList();
        }
        final int val = values.get(index);
        final List<List<String>> subsets = powerSet(values, index + 1);
        final List<List<String>> returnSet = new ArrayList<>();
        returnSet.add(Arrays.asList(String.valueOf(val)));
        returnSet.addAll(subsets);
        for (final List<String> subsetValues : subsets) {
            for (final String subsetValue : subsetValues) {
                returnSet.add(Arrays.asList(val + ", " + subsetValue));
            }
        }
        return returnSet;
    }
}
