package com.algos.questions.crackcode;

/**
 * User: harit
 */
public class Parenthesis {
    public static void main(String[] args) {
        printPara(4);
    }

    public static void printPara(int count) {
        final char[] str = new char[2 * count];
        printPara(count, count, str, 0);
    }

    private static void printPara(int l, int r, char[] str, int index) {
        if (l < 0 || r < l) {
            throw new IllegalStateException("not a vaid state");
        }
        if (l == 0 & r == 0) {
            System.out.println(str);
        } else {
            if (l > 0) {
                str[index] = '(';
                printPara(l - 1, r, str, index + 1);
            }
            if (r > l) {
                str[index] = ')';
                printPara(l, r - 1, str, index + 1);
            }
        }
    }
}
