package com.algos.questions.mycareerstack;

/**
 * @author Harit Himanshu
 */
public class ReverseDoublyLinkedList {
    public static DoublyLinkedListNode reverse(DoublyLinkedListNode head) {
        if (head == null) {
            throw new NullPointerException("Doubly Linked list can not be empty or null");
        }
        DoublyLinkedListNode node = head;
        DoublyLinkedListNode prev = null;
        while (node != null) {
            DoublyLinkedListNode next = node.getNext();
            node.setNext(prev);
            node.setPrev(next);
            prev = node;
            node = next;
        }
        return prev;
    }

    public static void printDoublyLinkedList(DoublyLinkedListNode head) {
        if (head == null) {
            throw new NullPointerException("Doubly Linked list can not be empty or null");
        }
        DoublyLinkedListNode node = head;

        System.out.print("NULL <=> ");
        while (node != null) {
            System.out.print(node.getValue() + " <=> ");
            node = node.getNext();
        }
        System.out.println("NULL");
    }
}

class DoublyLinkedListNode {
    private int value;
    private DoublyLinkedListNode prev;
    private DoublyLinkedListNode next;

    public DoublyLinkedListNode(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public DoublyLinkedListNode getPrev() {
        return prev;
    }

    public void setPrev(DoublyLinkedListNode prev) {
        this.prev = prev;
    }

    public DoublyLinkedListNode getNext() {
        return next;
    }

    public void setNext(DoublyLinkedListNode next) {
        this.next = next;
    }
}
