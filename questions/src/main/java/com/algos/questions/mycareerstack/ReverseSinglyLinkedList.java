package com.algos.questions.mycareerstack;

/**
 * @author Harit Himanshu
 * Reference: http://prepare.hackerearth.com/question/66/reverse-a-linked-list-without-making-recursive-calls/
 */
public class ReverseSinglyLinkedList {

    public static LinkedListNode reverse(LinkedListNode head) {
        if (head == null) {
            throw new NullPointerException("LinkedList can not be empty or null");
        }
        LinkedListNode node = head;
        LinkedListNode prev = null;

        while (node != null) {
            LinkedListNode next = node.getNext();
            node.setNext(prev);
            prev = node;
            node = next;
        }
        return prev;
    }

    public static void printLinkedList(LinkedListNode head) {
        if (head == null) {
            throw new NullPointerException("LinkedList can not be empty or null");
        }
        LinkedListNode node = head;
        while (node != null) {
            System.out.print(node.getValue() + " -> ");
            node = node.getNext();
        }
        System.out.println("NULL");
    }
}

class LinkedListNode<Item> {
    private Item value;
    private LinkedListNode next;

    LinkedListNode(Item value) {
        this.value = value;
    }

    public Item getValue() {
        return value;
    }

    public void setNext(LinkedListNode next) {
        this.next = next;
    }

    public LinkedListNode getNext() {
        return next;
    }
}
