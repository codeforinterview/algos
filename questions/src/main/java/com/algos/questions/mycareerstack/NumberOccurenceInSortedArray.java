package com.algos.questions.mycareerstack;

/**
 * @author Harit Himanshu
 *         Reference - http://prepare.hackerearth.com/question/328/number-of-occurrences-of-a-number-in-a-sorted-array/
 */
public class NumberOccurenceInSortedArray {

    public static int getNumberOfOccurence(int value, int[] a, int lo, int hi) {
        if (lo > hi) {
            return 0;
        }
        if (lo == hi) {
            return value == a[lo] ? 1 : 0;
        }

        int mid = (lo + hi) / 2;
        int left = 0;
        int right = 0;

        if (value <= a[mid]) {
            left = getNumberOfOccurence(value, a, lo, mid - 1);
        }

        if (value >= a[mid + 1]) {
            right = getNumberOfOccurence(value, a, mid + 1, hi);
        }

        return (value == a[mid] ? 1 : 0) + left + right;
    }
}
