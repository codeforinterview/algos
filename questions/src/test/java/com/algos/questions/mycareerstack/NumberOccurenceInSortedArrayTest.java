package com.algos.questions.mycareerstack;

import org.junit.Test;

import static junit.framework.Assert.assertEquals;

/**
 * @author Harit Himanshu
 */
public class NumberOccurenceInSortedArrayTest {

    @Test(expected = NullPointerException.class)
    public void testGetNumberOfOccurenceWithNullArray() throws Exception {
        NumberOccurenceInSortedArray.getNumberOfOccurence(5, null, 1, 5);
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void testGetNumberOfOccurenceWithWithIllegalIndex() {
        NumberOccurenceInSortedArray.getNumberOfOccurence(5, new int[]{}, 1, 2);
    }

    @Test
    public void testGetNumberOfOccurenceWithSwappedIndex() {
        assertEquals(NumberOccurenceInSortedArray.getNumberOfOccurence(1, new int[]{1, 1}, 2, 1), 0);
    }

    @Test
    public void testGetNumberOfOccurence() {
        int[] a = new int[]{1, 2, 2, 3, 3, 3, 4, 4, 4, 4};
        assertEquals(NumberOccurenceInSortedArray.getNumberOfOccurence(0, a, 0, a.length-1), 0);
        assertEquals(NumberOccurenceInSortedArray.getNumberOfOccurence(1, a, 0, a.length-1), 1);
        assertEquals(NumberOccurenceInSortedArray.getNumberOfOccurence(2, a, 0, a.length-1), 2);
        assertEquals(NumberOccurenceInSortedArray.getNumberOfOccurence(3, a, 0, a.length-1), 3);
        assertEquals(NumberOccurenceInSortedArray.getNumberOfOccurence(4, a, 0, a.length-1), 4);
    }
}
