package com.algos.questions.mycareerstack;

/**
 * @author Harit Himanshu
 * Reference: http://prepare.hackerearth.com/question/317/reverse-characters-of-each-word-in-a-sentence/
 */
public class ReverseCharactersEachWordSinglyLinkedList {
    private Node start;
    private Node end;

    public static void main(String args[]) {
        ReverseCharactersEachWordSinglyLinkedList list = new ReverseCharactersEachWordSinglyLinkedList();
        Node head = list.buildLinkedList("my career stack");
        list.show(head);
        Node reverse = list.reverse(head);
        list.show(reverse);
    }

    private Node buildLinkedList(java.lang.String input) {
        for (int i = 0; i < input.length(); i++) {
            Node node = new Node(input.charAt(i));
            if (start == null) {
                start = node;
                end = start;
            } else {
                end.setNext(node);
                end = node;
            }
        }
        return start;
    }

    public Node reverse(Node head) {
        if (head == null) {
            System.out.println("head node is null");
            return null;
        }
        Node prev = null;
        Node last = null;
        Node node = head;

        while (node != null) {
            if (node.getValue() == ' ') {
                break;
            }
            Node next = node.getNext();
            node.setNext(prev);
            prev = node;
            node = next;
        }

        if (node != null) {
            last = prev;
            head.setNext(node);
            if (node.getNext() != null) {
                node.setNext(reverse(node.getNext()));
            }
        }

        if (last == null) {
            return prev;
        }
        return last;
    }

    public void show(Node head) {
        Node node = head;
        while (node != null) {
            System.out.print(node.getValue() + "->");
            node = node.getNext();
        }
        System.out.println("NULL");
    }

    private static class Node {
        private Character value;
        private Node next;

        private Node(Character value) {
            this.value = value;
        }

        public Character getValue() {
            return value;
        }

        public void setNext(Node node) {
            next = node;
        }

        public Node getNext() {
            return next;
        }
    }
}
