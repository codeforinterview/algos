package com.algos.questions.mycareerstack;

import org.junit.Before;
import org.junit.Test;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNull;

/**
 * @author Harit Himanshu
 */
public class ReverseDoublyLinkedListTest {

    private DoublyLinkedListNode node1;

    @Before
    public void setup() {
        node1 = new DoublyLinkedListNode(1);
        DoublyLinkedListNode node2 = new DoublyLinkedListNode(2);
        DoublyLinkedListNode node3 = new DoublyLinkedListNode(3);
        DoublyLinkedListNode node4 = new DoublyLinkedListNode(4);

        node1.setNext(node2);

        node2.setPrev(node1);
        node2.setNext(node3);

        node3.setPrev(node2);
        node3.setNext(node4);

        node4.setPrev(node3);
        // creates doubly linked list NULL <=> 1 <=> 2 <=> 3 <=> 4 <=> NULL
    }

    @Test(expected = NullPointerException.class)
    public void testReverseWithEmptyList() throws Exception {
        ReverseDoublyLinkedList.reverse(null);
    }

    @Test
    public void testReverseWithFourNodes() {
        DoublyLinkedListNode reversed = ReverseDoublyLinkedList.reverse(node1);

        assertEquals(reversed.getValue() ,4);
        assertEquals(reversed.getNext().getPrev().getValue(), 4);

        assertEquals(reversed.getNext().getValue(), 3);
        assertEquals(reversed.getNext().getNext().getPrev().getValue(), 3);

        assertEquals(reversed.getNext().getNext().getValue(), 2);
        assertEquals(reversed.getNext().getNext().getNext().getPrev().getValue(), 2);

        assertEquals(reversed.getNext().getNext().getNext().getValue(), 1);

        assertNull(reversed.getNext().getNext().getNext().getNext());
    }
}
