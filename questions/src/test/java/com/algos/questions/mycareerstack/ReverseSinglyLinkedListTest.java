package com.algos.questions.mycareerstack;

import org.junit.Before;
import org.junit.Test;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNull;

/**
 * @author Harit Himanshu
 */
public class ReverseSinglyLinkedListTest {
    private LinkedListNode<Integer> node1;

    @Before
    public void setup() {
        node1 = new LinkedListNode<Integer>(1);
        LinkedListNode node2 = new LinkedListNode<Integer>(2);
        LinkedListNode node3 = new LinkedListNode<Integer>(3);
        LinkedListNode node4 = new LinkedListNode<Integer>(4);
        node1.setNext(node2);
        node2.setNext(node3);
        node3.setNext(node4);

        // creates a linked list as 1->2->3->4
    }

    @Test(expected = NullPointerException.class)
    public void testReverseWithEmptyLinkedList() throws Exception {
        ReverseSinglyLinkedList.reverse(null);
    }

    @Test
    public void testReverseWithFourNodes() {
        LinkedListNode reversed = ReverseSinglyLinkedList.reverse(node1);
        assertEquals(reversed.getValue(), 4);
        assertEquals(reversed.getNext().getValue(), 3);
        assertEquals(reversed.getNext().getNext().getValue(), 2);
        assertEquals(reversed.getNext().getNext().getNext().getValue(), 1);
        assertNull(reversed.getNext().getNext().getNext().getNext());
    }

    @Test
    public void testReverseSentenceWithSpaces() {
        LinkedListNode<Character> n1 = new LinkedListNode<Character>('m');
        LinkedListNode<Character> n2 = new LinkedListNode<Character>('y');
        LinkedListNode<Character> n3 = new LinkedListNode<Character>(' ');
        LinkedListNode<Character> n4 = new LinkedListNode<Character>('c');
        LinkedListNode<Character> n5 = new LinkedListNode<Character>('a');
        LinkedListNode<Character> n6 = new LinkedListNode<Character>('r');
        LinkedListNode<Character> n7 = new LinkedListNode<Character>('r');
        LinkedListNode<Character> n8 = new LinkedListNode<Character>('e');
        LinkedListNode<Character> n9 = new LinkedListNode<Character>('r');
        LinkedListNode<Character> n10 = new LinkedListNode<Character>(' ');
        LinkedListNode<Character> n11 = new LinkedListNode<Character>('s');
        LinkedListNode<Character> n12 = new LinkedListNode<Character>('t');
        LinkedListNode<Character> n13 = new LinkedListNode<Character>('a');
        LinkedListNode<Character> n14 = new LinkedListNode<Character>('c');
        LinkedListNode<Character> n15 = new LinkedListNode<Character>('k');

        n1.setNext(n2);
        n2.setNext(n3);
        n3.setNext(n4);
        n4.setNext(n5);
        n5.setNext(n6);
        n6.setNext(n7);
        n7.setNext(n8);
        n8.setNext(n9);
        n8.setNext(n9);
        n9.setNext(n10);
        n10.setNext(n11);
        n11.setNext(n12);
        n12.setNext(n13);
        n13.setNext(n14);
        n14.setNext(n15);

        ReverseSinglyLinkedList.printLinkedList(n1);
        ReverseSinglyLinkedList.printLinkedList(ReverseSinglyLinkedList.reverse(n1));
    }
}
